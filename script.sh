#!/bin/bash
#set -x 
function check_url () {
 urlstatus=$(curl -s -o /dev/null -w "%{http_code}" $1)
 if [ "$urlstatus" != "400-511" ]
 then
 echo "Ok"
 else
 echo "Error"
 fi
}
exit
#set +x
